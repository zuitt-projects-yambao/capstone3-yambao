import { Table } from 'react-bootstrap';
import History from '../components/History';
import { useState, useEffect} from 'react';

export default function OrderHistory(){

	const [orders, setOrders] = useState('');

	useEffect(() => {
		fetch('https://polar-wildwood-56213.herokuapp.com/orders/history', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			setOrders(data.map(order => {
				return (
					<History key={order._id} historyProp={order} />
				);
			}))
		})
	}, [])





	return (
		<Table striped bordered hover size="sm">
		  <thead>
		    <tr>
		      <th>Name</th>
		      <th>Order</th>
		      <th>Price</th>
		      <th>Date</th>
		    </tr>
		  </thead>
		  <tbody>
		    
		    {orders}

		  </tbody>
		</Table>
	)

}