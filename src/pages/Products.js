import ProductCard from '../components/ProductCard'
import {useState, useEffect} from 'react';
import {Row} from 'react-bootstrap';

// import {Card, Button, Col, Row} from 'react-bootstrap';

// Note: Create a new component for placeholder of all Products

// This section will be logic of the products and fetching

export default function Products () {
	
	const[products, setProducts] = useState([]);

	useEffect(() => {
		fetch('https://polar-wildwood-56213.herokuapp.com/products')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product} />
				);
			}))
		})
	}, [])



	
	return (
		<>
			<Row className="my-3">
			<h1>Products</h1>
			{products}
			</Row>
		</>
	)
}