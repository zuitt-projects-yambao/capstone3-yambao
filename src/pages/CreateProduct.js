import {Form, Button} from 'react-bootstrap';
import {useState} from 'react';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';

export default function CreateProduct () {

	const history = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [stocks, setStocks] = useState('');

	function createProduct(e){
		e.preventDefault();

		fetch('https://polar-wildwood-56213.herokuapp.com/products/', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				stocks: stocks
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Product Added Successfully!",
					icon: "success",
					text: "You have Successfully created a new Product"	
				})

				history("/admin")
			}
			else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please contact admin"
				})
			}
		})
	}



	return (
		<Form onSubmit={e => createProduct(e)}>
			<h1>Create New Product</h1>



			<Form.Group className="mt-3" controlId="productName">
				<Form.Label>Name</Form.Label>
				<Form.Control 
					type = "text"
					value={name}
					onChange={e => setName(e.target.value)}
					placeholder = "Enter Product Name"
					required
				/>
			</Form.Group>

			<Form.Group className="mt-3" controlId="description">
				<Form.Label>Description</Form.Label>
				<Form.Control 
					type = "text"
					value={description}
					onChange={e => setDescription(e.target.value)}
					placeholder = "Enter Description"
					required
				/>
			</Form.Group>

			<Form.Group className="mt-3" controlId="price">
				<Form.Label>Price</Form.Label>
				<Form.Control 
					type = "number"
					value={price}
					onChange={e => setPrice(e.target.value)}
					placeholder = "Enter Price"
					required
				/>
			</Form.Group>

			<Form.Group className="mt-3" controlId="stock">
				<Form.Label>Stock</Form.Label>
				<Form.Control 
					type = "number"
					value={stocks}
					onChange={e => setStocks(e.target.value)}
					placeholder = "Enter Stock"
					required
				/>
			</Form.Group>


			<Button className="mt-3" variant="primary" type="submit" id="submitBtn">Create</Button>

		</Form>
	)
}