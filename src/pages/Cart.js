import CartView from '../components/CartView'
import { Table, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Cart () {

	const history = useNavigate();

	const cartListOfID = [];

	const [cartList, setCartList] = useState(0)
	
	const [cart, setCart] = useState('');
	const [cartTotalPrice, setCartTotalPrice] = useState(0)
	const [cartId, setCartId] = useState('');

	const [productId, setProductId] = useState('');
	const [userId, setUserId] = useState('');
	const [orderedByUser, setOrderedByUser] = useState(0);
	const [totalPrice, setTotalPrice] = useState(0);
	const [payment, setPayment] = useState('wallet')

	useEffect(() => {
		fetch('https://polar-wildwood-56213.herokuapp.com/cart', {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			
			setCartList(data.length);

			// 	 console.log(data.length)
			// })
			// console.log(cartList)

			// numbers.forEach(function(number) {
			//     console.log(number);
			// });

			// setCartId(data.id);
			// console.log(cartId);

			setCart(data.map(cart => {
				console.log(cart._id)
				// console.log(data.size)

				// setCartList([...cartList, cart._id]);
				// console.log(`CART LIST 1st: ${cartList}`);
				
				setProductId(cart.productId);
				setUserId(cart.userId);
				setOrderedByUser(cart.totalOrder);


				console.log(orderedByUser);

				// console.log(cartList);

				// console.log(cart._id)
				setCartId(cart._id);
				console.log(cartId);

				// setCartList([...cartList, cartId]);
				// console.log(cartList);

			return (

				<CartView key={cart._id} cartProp={cart}/>
			
			);
		}))
	})
		
	}, [cartId]) 

	 console.log(cartList);


	 // console.log(`CART LIST: ${cartList}`);

	 function checkOutOrder(){

	 	let list = [];

	 	for(let i = 1; i <= cartList; i++) {
	 		let cartId = document.getElementsByTagName("tr")[i].id;
	 		console.log(cartId);
	 		list.push(cartId);
	 	}

	 	console.log(list);

	 	console.log(list[0])

	 	console.log(list.length)

	 	for(let i = 0; i < list.length; i++){

	 		console.log(`http://localhost:4000/orders/checkOut/${list[i]}`);

	 	 	fetch(`https://polar-wildwood-56213.herokuapp.com/orders/checkOut/${list[i]}`, {
	 			method: 'POST',
	 			headers: {
	 				"Content-type" : "application/json",
	 				Authorization : `Bearer ${localStorage.getItem("token")}`
	 			},
	 			body: JSON.stringify({
	 				userId: userId,
	 				productId: productId,
	 				orderedByUser: orderedByUser,
	 				totalPrice: totalPrice,
	 				payment: payment
	 			})
	 		})
	 		.then(res => res.json())
	 		.then(data => {
	 			console.log(data);

	 			if(data) {
	 				Swal.fire({
					title: "Order Added Successfully!",
					icon: "success",
				})

					history("/history")
	 			}
	 			else {
	 				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please contact admin"
				})
	 			}
	 		})	

	 	}

	 }


	// function checkOutOrder(){
	// 	fetch(`http://localhost:4000/orders/checkOut/${cartId}`, {
	// 		method: 'POST',
	// 		headers: {
	// 			"Content-type" : "application/json",
	// 			Authorization : `Bearer ${localStorage.getItem("token")}`
	// 		},
	// 		body: JSON.stringify({
	// 			userId: userId,
	// 			productId: productId,
	// 			orderedByUser: orderedByUser,
	// 			totalPrice: totalPrice,
	// 			payment: payment
	// 		})
	// 	})
	// 	.then(res => res.json())
	// 	.then(data => {
	// 		console.log(data);
	// 	}
	// }

	return (
		<>
		<Table striped bordered hover size="sm">
		  <thead>
		    <tr>
		      <th>Name</th>
		      <th>Price</th>
		      <th>Order</th>
		      <td>Sub Total</td>
		      <td></td>
		    </tr>
		  </thead>
		  
			{cart}

		</Table>

		<Button variant="success" onClick={checkOutOrder}>Check Out</Button>
		</>
	)
}