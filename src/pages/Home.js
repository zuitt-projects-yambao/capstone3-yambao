import Carousel from 'react-bootstrap/Carousel';

export default function Home () {
	return (
		<Carousel>
		  <Carousel.Item>
		    <img
		      className="d-block"
		      style={{height: 500}}
		      src="https://static.fandomspot.com/images/01/11563/17-excalipoor-final-fantasy-weapon.jpg"
		      alt="First slide"
		    />
		    <Carousel.Caption>
		      <h3>First slide label</h3>
		      <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block"
		      style={{height: 500}}
		      src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTMCSUr2yQ_ZFX10sWIPURpXBhPwM2R50fu3g&usqp=CAU"
		      alt="Second slide"
		    />

		    <Carousel.Caption>
		      <h3>Second slide label</h3>
		      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block"
		      style={{height: 500}}
		      src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS_fJV66rsGj9jb2Zgyk66b8VQaDE7AaUrBHg&usqp=CAU"
		      alt="Third slide"
		    />

		    <Carousel.Caption>
		      <h3>Third slide label</h3>
		      <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		</Carousel>

		)
}