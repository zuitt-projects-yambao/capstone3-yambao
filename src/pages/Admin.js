import {Table, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom'
import AdminView from '../components/AdminView';

export default function Admin(){

	const [products, setProducts] = useState('');

	useEffect(() => {
		fetch('https://polar-wildwood-56213.herokuapp.com/products/all', {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)



			setProducts(data.map(product => {
		
				// console.log(cart);

			return (

				<AdminView key={product._id} allProductProp={product}/>
			
			);
		}))
	})
		
	}, []) 


	return(
		<>
		<Link className="btn btn-primary" to='/create'>Create</Link>
		<Table striped bordered hover variant="dark" size="sm">
		  <thead>
		    <tr>
		      <th>#</th>
		      <th>Name</th>
		      <th>Desciption</th>
		      <th>Price</th>
		      <th>Stocks</th>
		      <th></th>
		      <th></th>
		    </tr>
		  </thead>
		  <tbody>
		    {products}
		  </tbody>
		</Table>
		</>
	)
}