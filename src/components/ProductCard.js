import {Card, Button, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';


// Note: Create a new component for placeholder of all Products

// This section will be logic of the products and fetching

export default function Products ({productProp}) {
	
	const {name, description, price, stocks, _id} = productProp

	console.log(productProp);

	console.log(name);

	return (
			
			<Card className="p-0 mt-3">
			  <Card.Header as="h5">{name}</Card.Header>
			  <Card.Body>
			    <Card.Title>Description</Card.Title>
			    <Card.Text>
			      {description}
			    </Card.Text>
			    <Card.Text>Gil: {price}</Card.Text>
				<Card.Text>Available: {stocks}</Card.Text>
			    <Button variant="primary" as={Link} to={`/products/${_id}`}>See Details</Button>
			  </Card.Body>
			</Card>
			


			
			/*<Col className="mt-2" xs={12} md={6}>
				<Card className="mx-2">
				  {/*<Card.Img variant="top" src="holder.js/100px180" />*
				  <Card.Body>
				    <Card.Title>{name}</Card.Title>
				    <Card.Subtitle>Description:</Card.Subtitle>
				    <Card.Text>{description}</Card.Text>
				    <Card.Text>Gil: {price}</Card.Text>
				    <Card.Text>Available: {stocks}</Card.Text>
				    <Button variant="primary" as={Link} to={`/products/${_id}`}>See Details</Button>
				  </Card.Body>
				</Card>
			</Col>*/

			

		
	)
}