import {  Button } from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import Swal from 'sweetalert2';
// import { useNavigate } from 'react-router-dom';

export default function CartView({cartProp}) {
	

	// const history = useNavigate();
	const {totalOrder, productId, _id} = cartProp;

	console.log(_id);

	const [order, setOrder] = useState(totalOrder);

	// console.log(order);
	// console.log(productId);


	const [cartId, setCartId] = useState(); 
	const [name, setName] = useState('');
	const [price, setPrice] = useState('');
	const [stocks, setStocks] = useState('');
	const [subTotal, setSubtotal] = useState(0);

	console.log(cartId);

	// console.log(`This is the price ${price}`);
	// console.log(`This is the order ${order}`);
	// console.log(setTotalPrice);
	// console.log(order);
	// console.log(stocks);

	useEffect(() => {
		// console.log(productId);

		fetch(`https://polar-wildwood-56213.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name);
			setPrice(data.price);
			setStocks(data.stocks);
			// setCartId(_id);
			setSubtotal(order * data.price)


		})
		
	}, [order, productId, _id ])



	const increment = () => {

		if(stocks > order){
			// console.log(stocks);

			fetch(`https://polar-wildwood-56213.herokuapp.com/cart/updateQuantity/${_id}`, {
				method: 'PUT',
				headers: {
					'Content-Type' : 'application/json',
					Authorization: `Bearer ${localStorage.getItem("token")}`
				},
				body: JSON.stringify({
					totalOrder: order + 1
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				if(data === true) {
					setOrder(order => order + 1)
					setSubtotal(subTotal => order * price)

					// console.log(order)
				}

				else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please contact admin"
					})
				}
			})

			
			
		}
	}

	const decrement = () => {
		if(order > 1){



			fetch(`https://polar-wildwood-56213.herokuapp.com/cart/updateQuantity/${_id}`, {
				method: 'PUT',
				headers: {
					'Content-Type' : 'application/json',
					Authorization: `Bearer ${localStorage.getItem("token")}`
				},
				body: JSON.stringify({
					totalOrder: order - 1
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				if(data === true) {
					setOrder(order - 1)
					setSubtotal(order * price)

					// console.log(order)
				}

				else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please contact admin"
					})
				}
			})


			
		}
	}

	const deleteItem = () => {

		console.log(`http://localhost:4000/cart/deleteCartItem/${_id}`);

		fetch(`https://polar-wildwood-56213.herokuapp.com/cart/deleteCartItem/${_id}`, {
			method : "DELETE",
			headers : {
			"Content-type" : "application/json",
			Authorization : `Bearer ${localStorage.getItem("token")}`
			}		
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){

				Swal.fire({
					title: "Deleted Item Successfully!",
					icon: "success",
				})

				console.log(_id);
				document.getElementById(`${_id}`).remove();
				// history("/cart");
				// v5: history/push("/endpoint")

			}

			else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please contact admin"
				})
			}
		})
	}
	

	return (
		
		  <tbody>
		    <tr id={_id}>
		      <td>{name}</td>
		      <td>{price}</td>
		      <td><Button onClick={decrement}>-</Button>
		    		<span className="mx-5">{order}</span>
		    		<Button onClick={increment}>+</Button></td>
		      <td>{subTotal}</td>	
		      <td><Button onClick={deleteItem}>Delete</Button></td>
		    </tr>
		  </tbody>
		
	)
}