import { useState, useEffect} from 'react';

export default function History({historyProp}){
	
	const {orderedByUser, productId, totalPrice, createdOn } = historyProp

	const [name, setName] = useState('')

	useEffect(() => {
		// console.log(productId);

		fetch(`https://polar-wildwood-56213.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			setName(data.name);

		})
	}, [productId])


	return ( 
		<tr>
	      <td>{name}</td>
	      <td>{orderedByUser}</td>
	      <td>{totalPrice}</td>
	      <td>{createdOn}</td>
	    </tr>
	)
}