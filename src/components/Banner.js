import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext';

export default function Banner({data}) {
	
	const {user, setUser} = useContext(UserContext);

	console.log(data)
	const {title, content, destination, label} = data;

	return(
		<Row>
			<Col className = "p-5">
				<h1>{title}</h1>
				<p>{content}</p>
				{
					(user.isAdmin) ?
						<Button as={Link} to='/admin'>{label}</Button>
					:
						<Button as={Link} to='/products'>{label}</Button>
				}
				
			</Col>
		</Row>

	)


	// return (
	// 	<Row className="p-5">
	// 		<Col>
	// 			<h1>Zuitt Coding Bootcamp</h1>
	// 			<p>Opportunities for everyone, everywhere</p>
	// 			<Button variant="primary">Enroll Now!</Button>
	// 		</Col>
	// 	</Row>
	// )
}


// import { Row, Col, Button } from 'react-bootstrap';
// import {Link} from 'react-router-dom';

// export default function Banner({data}){

// 	console.log(data)
// 	const {title, content, destination, label} = data;

// 	return(
// 		<Row>
// 			<Col className = "p-5">
// 				<h1>{title}</h1>
// 				<p>{content}</p>

// 				<Button as={Link} to={destination}>{label}</Button>
// 			</Col>
// 		</Row>

// 	)
// }
