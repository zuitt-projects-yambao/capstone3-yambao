import {useContext} from 'react';
import {Navbar, Container, Nav} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar(props) {
	
	const {user} = useContext(UserContext);

	console.log(user);

	return (

		<Navbar bg="light" expand="lg">
		  <Container>
		    <Navbar.Brand as={Link} to="/">FFI STORE</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="me-auto">

		      	{
		      		(user.isAdmin) ?
		      		<Nav.Link as={Link} to="/admin">Admin Control Page</Nav.Link>
		      		:
		      		(user.isAdmin === false) ?
		      		<>
		      		<Nav.Link as={Link} to="/">Home</Nav.Link>
			        <Nav.Link as={Link} to="/products">Products</Nav.Link>
			        <Nav.Link as={Link} to="/cart">Cart</Nav.Link>
			        <Nav.Link as={Link} to="/history">History</Nav.Link>
			        </>
		      		:
		      		<>
		      		<Nav.Link as={Link} to="/">Home</Nav.Link>
			        <Nav.Link as={Link} to="/products">Products</Nav.Link>
			        </>
		      	}
		        
		        
		        {
		        (user.id !== null) ?

		        <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
		        
		        
		        :
		        <>
		        <Nav.Link as={Link} to="/register">Register</Nav.Link>
		        
		        <Nav.Link as={Link} to="/login">Login</Nav.Link>
		        </>
		        
		    	}
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>

	)
}

// Note: For editing User can access HOME, PRODUCTS, CART

// Note: For Admin Add, Edit Function