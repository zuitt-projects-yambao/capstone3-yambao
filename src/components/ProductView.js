import { useState, useEffect, useContext} from 'react';
import {Card, Button} from 'react-bootstrap';
import { useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2';

export default function ProductView() {
	
	const {user} = useContext(UserContext);

	const history = useNavigate();

	const [count, setCount] = useState(1);


	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [stocks, setStocks] = useState('');
	const {productId} = useParams();

	console.log(productId);

	useEffect(() => {
		console.log(productId);

		fetch(`https://polar-wildwood-56213.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setStocks(data.stocks);

		})
	}, [productId])


	const addToCart = () => {

		fetch(`https://polar-wildwood-56213.herokuapp.com/cart/addToCart/${productId}`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				totalOrder: count
			})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){

				Swal.fire({
					title: "Added To Cart",
					icon: "success",
					
				})


				history("/products");
				// v5: history/push("/endpoint")

			}

			else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please contact admin"
				})
			}
		})

	}


	const increment = () => {
		if(stocks > count){
			setCount(count + 1)

			console.log(count);
		}
	}

	const decrement = () => {
		if(count > 1){
			setCount(count - 1)
		}
	}

	return (
		<>
		
		<Card className="text-center">
		  <Card.Header><h1>Product Details</h1></Card.Header>
		  <Card.Body>
		    <Card.Title>{name}</Card.Title>
		    <Card.Text>
		      {description}
		    </Card.Text>
		    <Card.Text>
		      Gil: {price}
		    </Card.Text>
		    <Card.Text>Available: {stocks}</Card.Text>
		    {
		    	(user.id !==null && user.isAdmin === false) ?
		    	<>
		    		<Button onClick={decrement}>-</Button>
		    		<span className="mx-5">{count}</span>
		    		<Button onClick={increment}>+</Button>
		    		<br />
		    		<Button className="mt-3" variant="primary" onClick={addToCart}>Add to Cart</Button>
		    	</>
		    	:
		    	(user.id !==null && user.isAdmin) ?
		    		<Link className="btn btn-primary" to='/products'>Go Back</Link>
		    	:
		    		<Link className="btn btn-primary" to='/login'>Login</Link>
		    }
		  </Card.Body>
		</Card>
		</>

		/*<Card>
		<h1>Product Details</h1>
		  <Card.Img variant="top" src="holder.js/100px180" />
		  <Card.Body>
		    <Card.Title>{name}</Card.Title>
		    <Card.Subtitle>Description</Card.Subtitle>
		    <Card.Text>{description}</Card.Text>
		    <Card.Text>Gil: {price}</Card.Text>
		    <Card.Text>Available: {stocks}</Card.Text>
		    {
		    	(user.id !==null && user.isAdmin === false) ?
		    	<>
		    		<Button onClick={decrement}>-</Button>
		    		<span className="mx-5">{count}</span>
		    		<Button onClick={increment}>+</Button>
		    		<br />
		    		<Button className="mt-3" variant="primary" onClick={addToCart}>Add to Cart</Button>
		    	</>
		    	:
		    	(user.id !==null && user.isAdmin) ?
		    		<Link className="btn btn-primary" to='/products'>Go Back</Link>
		    	:
		    		<Link className="btn btn-primary" to='/login'>Login</Link>
		    }
    
		  </Card.Body>
		</Card>*/
		
	)
}